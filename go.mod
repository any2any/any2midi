module any2midi

go 1.13

require (
	github.com/algoGuy/EasyMIDI v0.0.0-20180322051653-708ca39e7399
	gitlab.com/any2any/anylib v1.0.6
)
