package main

import (
	"bufio"
	"math"
	"os"

	"github.com/algoGuy/EasyMIDI/smf"
	"github.com/algoGuy/EasyMIDI/smfio"
	"gitlab.com/any2any/anylib"
)

const (
	stepSize uint32  = 240
	min      float64 = 20
	max      float64 = 100
)

func main() {
	cfg := anylib.LoadAnyConfig("any2midi", "midi.mid", ".mid")

	cfg.Logger.Log("Preparing midi file...")

	division, err := smf.NewDivision(960, smf.NOSMTPE)
	cfg.Logger.Must("Error while creating division", err)

	midi, err := smf.NewSMF(smf.Format0, *division)
	cfg.Logger.Must("Error while creating smf", err)

	track := &smf.Track{}

	err = midi.AddTrack(track)
	cfg.Logger.Must("Error while adding track", err)

	cfg.Logger.Log("Rendering notes...")

	err = anylib.ReadFileInChunks(cfg.InputFile, 1, cfg.IgnoreZero, func(b []byte) {
		not := byteToNote(b[0])

		event, err := smf.NewMIDIEvent(0, smf.NoteOnStatus, 0x00, not, 0x50)
		cfg.Logger.Must("Error while adding note", err)
		track.AddEvent(event)

		event, err = smf.NewMIDIEvent(stepSize, smf.NoteOnStatus, 0x00, not, 0x00)
		cfg.Logger.Must("Error while adding end note", err)
		track.AddEvent(event)
	})
	cfg.Logger.Must("Error while rendering notes", err)

	event, err := smf.NewMetaEvent(0, smf.MetaEndOfTrack, []byte{})
	cfg.Logger.Must("Error while writing final note", err)

	err = track.AddEvent(event)
	cfg.Logger.Must("Error while writing final note", err)

	cfg.Logger.Log("Writing file...")

	outputMidi, err := os.Create(cfg.OutputFile)
	cfg.Logger.Must("Error while creating output file", err)
	defer outputMidi.Close()

	writer := bufio.NewWriter(outputMidi)
	smfio.Write(writer, midi)
	writer.Flush()

	cfg.Logger.Log("Completed")
}

func byteToNote(b byte) uint8 {
	num := int(b)

	output := min + ((max-min)/(256-0))*(float64(num)-0)

	return uint8(math.Round(output))
}
